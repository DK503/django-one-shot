from django.urls import path
from todos.views import show_todolist, show_detail, create_todolist, update_todolist, delete_todolist, create_todoitem, update_todoitem


urlpatterns = [
    path("", show_todolist, name="show_todolist"),
    path("<int:id>", show_detail, name="show_detail"),
    path("create/", create_todolist, name="create_todolist"),
    path("<int:id>/update/", update_todolist, name="update_todolist"),
    path("<int:id>/delete/", delete_todolist, name="delete_todolist"),
    path("items/create/", create_todoitem, name="create_todoitem"),
    path("items/<int:id>/update/", update_todoitem, name="update_todoitem"),
]
